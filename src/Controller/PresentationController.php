<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;



class PresentationController extends AbstractController
{

    /**
     * @Route("/presentation",name="presentation")
     */
    public function presentation()
    {
        $tab=[1,2,3,34,123,12345,12,42];
        return $this->render(
            "presentation.html.twig",
            [
                "prenom" => "simplon",
                "nom" => "lyon",
                "tableau"=> $tab
            ]
        );
    }
}