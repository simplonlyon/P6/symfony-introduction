<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Person;

/**
 * Les contrôleurs en Symfony vont plus ou moins représenter nos "pages".
 * Ils sont là pour faire la lien entre la vue, le modèle et le routeur.
 * Ils ne doivent pas contenir beaucoup d'algorithmie, celle ci 
 * sera plutôt dans la couche métier du modèle (donc dans d'autres
 * classes).
 * Personnelement je préfère avoir une classe controller par page.
 * Il est préférable que les contrôleurs héritent de la classe
 * AbstractController ou Controller
 */
class FirstController extends AbstractController {
    /*
    On utilise l'annotation @Route pour indiquer que la méthode
    juste en dessous sera à déclencher lorsque l'on tentera d'accéder
    à l'url indiquée entre les parenthèses.
    Le name de cette route sera utilisé pour faire les liens et les
    redirections.
    Les annotation ont une syntaxe bien spécifique, il faut qu'elles
    soient dans de la PhpDoc, donc un commentaire avec deux étoiles après le slash
    */
    /**
     * @Route("/", name="homepage")
     */
    public function index() {
        $nombre = 2;
        $tableau = ["ga", "zo", "bu"];

        $instance = new Person("Simplon", "José", 45);
        /* On utilise la méthode render() du controller pour dire à
        la méthode d'interpréter le template qu'on lui donne en premier argument.
        Le deuxième argument est un tableau associatif qui indiquera les
        variables que l'on rend accessible à l'intérieur du template.
        (la clef est le nom de la variable dans le template, la valeur
        sera la valeur de cette variable "clef" => "valeur")
        */
        return $this->render("first.html.twig", 
            ["variable" => "je viens du contrôleur",
            "autre" => $nombre,
            "tableau" => $tableau,
            "person" => $instance]
        );
    }

}