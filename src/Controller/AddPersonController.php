<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Person;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/add-person")
 */
class AddPersonController extends AbstractController {

    /**
     * @Route("/", name="add_person", , methods={"GET"})
     */
    public function index() {
    
        return $this->render("add-person.html.twig", []);
    }

    /**
     * @Route("/", name="add_person_post", methods={"POST"})
     */
    public function addPerson(Request $req) {
        $name = $req->get("name");
        $surname = $req->get("surname");
        $age =  $req->get("age");

        $person = new Person($name, $surname,$age );

        return $this->render("add-person.html.twig", [
            "person" => $person
        ]);
    }
}