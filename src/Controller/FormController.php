<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;


class FormController extends AbstractController {

    /**
     * @Route("/form", name="form")
     */
    public function index(Request $request) {
        $requestInput = $request->get("input");

        return $this->render("form.html.twig", [
            "variableInput" => $requestInput
        ]);
    }
}