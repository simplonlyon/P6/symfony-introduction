# Introduction à Symfony

Projet d'introduction au framework Symfony 4.1 pour la promo 6 de Simplon Lyon

## Installation / Utilisation du projet :
1. Faire un `git clone https://gitlab.com/simplonlyon/P6/symfony-introduction.git`
2. Ensuite on va dans le dossier `cd symfony-introduction`
3. On installe les dépendances `composer install`
4. On lance docker `docker-compose up`
5. On navigue sur http://localhost:8080